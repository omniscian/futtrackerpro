//
//  +String.swift
//  FUTTrackerPro
//
//  Created by Ian Houghton on 19/08/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import UIKit

extension String {
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
}
