//
//  +UITextField.swift
//  FUTTrackerPro
//
//  Created by Ian Houghton on 08/09/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import UIKit

extension UITextField {
    func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validated(self.text!)
    }
}
