//
//  FirebaseRepo.swift
//  FUTTrackerPro
//
//  Created by Ian Houghton on 21/09/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import Foundation

class FirebaseRepo {
    
    static let shared = FirebaseRepo()
    
    //Initializer access level change now
    private init(){}
}
