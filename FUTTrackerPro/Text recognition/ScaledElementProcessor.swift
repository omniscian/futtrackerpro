//
//  ScaledElementProcessor.swift
//  FUTTrackerPro
//
//  Created by Ian Houghton on 16/08/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import Firebase

struct ScaledElement: Equatable {
    let frame: CGRect
    let shapeLayer: CALayer
    let elementText: String
}

private enum Constants {
    static let lineWidth: CGFloat = 3.0
    static let lineColor = UIColor.yellow.cgColor
    static let fillColor = UIColor.clear.cgColor
}

class ScaledElementProcessor {
    let vision = Vision.vision()
    var textRecognizer: VisionTextRecognizer!
    var factsElement: VisionTextElement?
    var matchElement: VisionTextElement?
    
    init() {
        self.textRecognizer = vision.onDeviceTextRecognizer()
    }
    
    func parseMatchFacts( in imageView: UIImageView,
                  callback: @escaping (_ matchFacts: [[ScaledElement]]) -> Void) {
        
        guard let image = imageView.image else {
            return
        }
        
        let visionImage = VisionImage(image: image)
        self.textRecognizer.process(visionImage) { result, error in
            guard
                error == nil,
                let result = result,
                !result.text.isEmpty
                else {
                    callback([])
                    return
            }
            var visionElements: [VisionTextElement] = []

            for i in 0..<result.blocks.count {
                let block = result.blocks[i]

                for j in 0..<block.lines.count {
                let line = block.lines[j]

                    for k in 0..<line.elements.count{
                        let element = line.elements[k]
                        if element.text == "FACTS" {
                            self.factsElement = element
                            if line.elements[k-1].text == "MATCH" {
                                self.matchElement = line.elements[k-1]
                            }
                            else {
                                callback([])
                            }
                        }

                        if let unwrappedFacts = self.factsElement, let unwrappedMatch = self.matchElement {
                            if element.frame.origin.y > unwrappedFacts.frame.origin.y && element.frame.origin.x > unwrappedMatch.frame.origin.x {
                                // Match fact element
                                visionElements.append(element)
                            }
                        }
                    }
                }
            }

            var remainingElements = visionElements.sorted(by: { $0.frame.origin.x < $1.frame.origin.x })

            var processedMatchFacts = self.processMatchFacts(remainingElements, image.size, imageView.frame, .red)
            let leftColumn = processedMatchFacts.0
            remainingElements = processedMatchFacts.1

            processedMatchFacts = self.processMatchFacts(remainingElements, image.size, imageView.frame, .yellow)
            remainingElements = processedMatchFacts.1

            processedMatchFacts = self.processMatchFacts(remainingElements, image.size, imageView.frame, .green)
            let rightColumn = processedMatchFacts.0
            remainingElements = processedMatchFacts.1

            callback([leftColumn, rightColumn])
        }
    }
    
    func parseMatchScores( in imageView: UIImageView,
                           homeTeamName: String,
                           awayTeamName: String,
                           callback: @escaping (_ homeTeamScore: String?, _ awayTeamScore: String?) -> Void) {
        
        guard let image = imageView.image, homeTeamName.count > 0, awayTeamName.count > 0 else {
            return
        }
        
        let visionImage = VisionImage(image: image)
        textRecognizer.process(visionImage) { result, error in
            guard
                error == nil,
                let result = result,
                !result.text.isEmpty
                else {
                    callback("", "")
                    return
            }
            
            var elementsToParse: [VisionTextElement] = []
            
            outerLoop: for i in 0..<result.blocks.count {
                let block = result.blocks[i]
                
                for j in 0..<block.lines.count {
                    let line = block.lines[j]
                
                    for k in 0..<line.elements.count{
                        let element = line.elements[k]
                        
                        if element.text.contains(homeTeamName.split(separator: " ")[0]) || element.text.contains(awayTeamName.split(separator: " ")[0]) {
                            elementsToParse = line.elements
                            break outerLoop
                        }
                    }
                }
            }
            
            elementsToParse = elementsToParse.sorted(by: { $0.frame.origin.x < $1.frame.origin.x })
            let numberElements = elementsToParse.filter { $0.text.isNumeric }
            
            if numberElements.count == 2 {
                callback(numberElements[0].text, numberElements[1].text)
            } else {
                callback(nil, nil)
            }
        }
    }
    
    private func processMatchFacts(_ elementsArray: [VisionTextElement], _ imageSize: CGSize, _ imageViewFrame: CGRect, _ strokeColour: UIColor) -> ([ScaledElement], [VisionTextElement]) {
        var startingPoint: CGFloat = 0.0
        var scaledElements: [ScaledElement] = []
        var elementsToRemove: [VisionTextElement] = []
        
        for visionTextElement in elementsArray {
            if startingPoint == 0 {
                startingPoint = visionTextElement.frame.origin.x
            }
            
            if visionTextElement.frame.origin.x <= (startingPoint + 400) && visionTextElement.frame.origin.x >= (startingPoint - 400) {
                let frame = self.createScaledFrame(
                    featureFrame: visionTextElement.frame,
                    imageSize: imageSize,
                    viewFrame: imageViewFrame)
                
                let shapeLayer = self.createShapeLayer(frame: frame, strokeColor: strokeColour)
                let scaledElement = ScaledElement(frame: frame, shapeLayer: shapeLayer, elementText: visionTextElement.text)
                scaledElements.append(scaledElement)
                elementsToRemove.append(visionTextElement)
            }
            else {
                break
            }
        }
        
        return (scaledElements, elementsArray.filter { !elementsToRemove.contains($0) })
    }
    
    private func createShapeLayer(frame: CGRect, strokeColor: UIColor?) -> CAShapeLayer {
        let bpath = UIBezierPath(rect: frame)
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.path = bpath.cgPath
        shapeLayer.strokeColor = strokeColor != nil ? strokeColor!.cgColor : Constants.lineColor
        shapeLayer.fillColor = Constants.fillColor
        shapeLayer.lineWidth = Constants.lineWidth
        return shapeLayer
    }
    
    private func createScaledFrame(featureFrame: CGRect, imageSize: CGSize, viewFrame: CGRect) -> CGRect {
        let viewSize = viewFrame.size
        
        let rView = viewSize.width / viewSize.height
        let rImage = imageSize.width / imageSize.height
        
        var scale: CGFloat
        if rView > rImage {
            scale = viewSize.height / imageSize.height
        } else {
            scale = viewSize.width / imageSize.width
        }
        
        let featureWidthScaled = featureFrame.size.width * scale
        let featureHeightScaled = featureFrame.size.height * scale
        
        let imageWidthScaled = imageSize.width * scale
        let imageHeightScaled = imageSize.height * scale
        
        let imagePointXScaled = (viewSize.width - imageWidthScaled) / 2
        let imagePointYScaled = (viewSize.height - imageHeightScaled) / 2
        let featurePointXScaled = imagePointXScaled + featureFrame.origin.x * scale
        let featurePointYScaled = imagePointYScaled + featureFrame.origin.y * scale
        
        return CGRect(x: featurePointXScaled,
                      y: featurePointYScaled,
                      width: featureWidthScaled,
                      height: featureHeightScaled)
    }
}
