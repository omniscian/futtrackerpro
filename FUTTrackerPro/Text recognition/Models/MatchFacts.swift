//
//  MatchFacts.swift
//  FUTTrackerPro
//
//  Created by Ian Houghton on 19/08/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import UIKit

struct MatchResults: Codable {
    var homeTeamMatchFacts: MatchFacts
    var awayTeamMatchFacts: MatchFacts
    
    init(homeTeamMatchFacts: MatchFacts, awayTeamMatchFacts: MatchFacts) {
        self.homeTeamMatchFacts = homeTeamMatchFacts
        self.awayTeamMatchFacts = awayTeamMatchFacts
    }
    
    func jsonObject() -> Data? {
        
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(self)
            return jsonData
        } catch {
            return nil
        }
    }
}

struct MatchFacts: Codable {
    var teamName: String?
    var score: String?
    var shots: String?
    var shotsOnTarget: String?
    var possession: String?
    var tackles: String?
    var fouls: String?
    var corners: String?
    var shotAccuracy: String?
    var passAccuracy: String?
    
    init(elements: [ScaledElement]) {
        // Team name parsing
        var remainingElements = elements
        
        let teamNames = self.parseTeamName(elements: remainingElements)
        self.teamName = teamNames.0
        remainingElements = teamNames.1
        
        let parsedElements = self.parseShots(elements: remainingElements)
        
        if parsedElements.count == 8 {
            self.shots = parsedElements[0]
            self.shotsOnTarget = parsedElements[1]
            self.possession = parsedElements[2]
            self.tackles = parsedElements[3]
            self.fouls = parsedElements[4]
            self.corners = parsedElements[5]
            self.shotAccuracy = parsedElements[6]
            self.passAccuracy = parsedElements[7]
        }
    }
    
    private func parseTeamName(elements: [ScaledElement]) -> (String, [ScaledElement]) {
        let nonNumberElements = elements.filter { !$0.elementText.isNumeric }
        guard let startingPoint = nonNumberElements.first?.frame.origin.y else {
            return ("", elements)
        }
        var teamName = ""
        
        var tempElements: [ScaledElement] = []
        for element in nonNumberElements {
            if element.frame.origin.y <= (startingPoint + 30) && element.frame.origin.y >= (startingPoint - 30) {
                tempElements.append(element)
            }
        }
        
        tempElements = tempElements.sorted(by: { $0.frame.origin.x < $1.frame.origin.x })
        
        for element in tempElements {
            teamName.append(element.elementText)
            if element != tempElements.last {
                teamName.append(" ")
            }
        }
        
        return (teamName, elements.filter { !tempElements.contains($0) })
    }
    
    private func parseShots(elements: [ScaledElement]) -> ([String]) {
        let numberElements = elements.filter { $0.elementText.isNumeric }

        var parsedString: [String] = []
        
        if numberElements.count > 0 {
            for i in 0...numberElements.count - 1 {
                
                let element = numberElements[i]
                var startingPoint: CGFloat = 0
                
                if element == numberElements.first {
                    startingPoint = numberElements.first?.frame.origin.y ?? 0
                }
                else {
                    startingPoint = numberElements[i - 1].frame.origin.y
                }
                
                if element.frame.origin.y <= (startingPoint + 15) && element.frame.origin.y >= (startingPoint - 15) {
                    parsedString.append(element.elementText);
                }
                else {
                    parsedString.append("")
                    parsedString.append(element.elementText);
                }
            }
        }
        
        return (parsedString)
    }
    
    mutating func setScore(score: String) {
        self.score = score
    }
}
