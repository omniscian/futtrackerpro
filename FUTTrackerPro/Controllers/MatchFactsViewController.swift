//
//  MatchFactsViewController.swift
//  FUTTrackerPro
//
//  Created by Ian Houghton on 22/08/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import UIKit

class MatchFactsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var homeTeamTextFields: [UITextField]!
    @IBOutlet var awayTeamTextFields: [UITextField]!
    @IBOutlet var homeTeamName: UILabel!
    @IBOutlet var awayTeamName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func configure(homeTeam: MatchFacts, awayTeam: MatchFacts) {
        self.homeTeamName.text = homeTeam.teamName
        self.awayTeamName.text = awayTeam.teamName
        
        let mirroredHomeTeam = Mirror(reflecting: homeTeam)
        let mirroredAwayTeam = Mirror(reflecting: awayTeam)
        
        for (index, attr) in mirroredHomeTeam.children.enumerated() {
            if index != 0 {
                let textField = self.homeTeamTextFields[index - 1]
                textField.text = attr.value as? String
                self.highlightIfNeeded(textField: textField)
            }
        }
        
        for (index, attr) in mirroredAwayTeam.children.enumerated() {
            if index != 0 {
                let textField = self.awayTeamTextFields[index - 1]
                textField.text = attr.value as? String
                self.highlightIfNeeded(textField: textField)
            }
        }
    }
    
    private func highlightIfNeeded(textField: UITextField) {
        if textField.text?.isEmpty ?? false {
            textField.layer.borderWidth = 3.0
            textField.layer.borderColor = UIColor.red.cgColor
        } else {
            textField.layer.borderWidth = 0.0
            textField.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.highlightIfNeeded(textField: textField)
    }
}
