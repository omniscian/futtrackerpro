//
//  ViewController.swift
//  FUTTrackerPro
//
//  Created by Ian Houghton on 16/08/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class MatchResultsViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    let processor = ScaledElementProcessor()
    var frameSublayer = CALayer()
    var matchFactsViewController: MatchFactsViewController?
    var homeMatchFacts: MatchFacts?
    var awayMatchFacts: MatchFacts?
    var selectedImage = UIImage()
    
    // Database
    let ref = Database.database().reference()
    let user = Auth.auth().currentUser
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        self.imageView.layer.addSublayer(self.frameSublayer)
        self.imageView.image = self.selectedImage.fixOrientation()
        self.processor.parseMatchFacts(in: self.imageView) { elements in
            elements.forEach() { column in
                column.forEach() { feature in
                    self.frameSublayer.addSublayer(feature.shapeLayer)
                }
            }
            
            if elements.count == 2{
                var homeArray = elements[0]
                var awayArray = elements[1]
                
                homeArray = homeArray.sorted(by: { $0.frame.origin.y < $1.frame.origin.y })
                awayArray = awayArray.sorted(by: { $0.frame.origin.y < $1.frame.origin.y })
                
                self.homeMatchFacts = MatchFacts(elements: homeArray)
                self.awayMatchFacts = MatchFacts(elements: awayArray)
                
                guard let homeTeamName = self.homeMatchFacts?.teamName,
                    let awayTeamName = self.awayMatchFacts?.teamName else {
                        return
                }
                
                self.processor.parseMatchScores(in: self.imageView, homeTeamName: homeTeamName, awayTeamName: awayTeamName) { homeScore, awayScore in
                    
                    if let home = homeScore, let away = awayScore {
                        self.homeMatchFacts?.setScore(score: home)
                        self.awayMatchFacts?.setScore(score: away)
                    }
                    
                    guard let homeFacts = self.homeMatchFacts,
                        let awayFacts = self.awayMatchFacts else {
                            return
                    }
                    
                    self.matchFactsViewController?.configure(homeTeam: homeFacts, awayTeam: awayFacts)
                }    
            }
        }
    }
    
    func configure(_ image: UIImage) {
        self.selectedImage = image
    }
    
    internal override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MatchFactsSegue" {
            self.matchFactsViewController = segue.destination as? MatchFactsViewController
        }
    }
    
    private func showGenericErrorAlert() {
        let alert = UIAlertController(title: "Error", message: "Unable to save results. Please try again.", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {_ in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func save(){
        
        guard  let homeFacts = self.homeMatchFacts, let awayFacts = self.awayMatchFacts, let uid = self.user?.uid  else {
            self.showGenericErrorAlert()
            return
        }
        
        let matchResults = MatchResults(homeTeamMatchFacts: homeFacts, awayTeamMatchFacts: awayFacts)
        guard let object = matchResults.jsonObject(), let jsonToSend = String(data: object, encoding: String.Encoding.utf8) else {
            self.showGenericErrorAlert()
            return
        }
        
        self.ref.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            if value?.count ?? 0 > 0 {
                self.ref.child("users").child(uid).updateChildValues([Date.currentTimeStamp: jsonToSend])
            } else {
                self.ref.child("users").child(uid).setValue([Date.currentTimeStamp: jsonToSend])
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    @IBAction func saveButtonPressed() {
        
        let shouldShow = UserDefaults.standard.bool(forKey: "DontShow")
        
        if !shouldShow {
            let alert = UIAlertController(title: "Warning", message: "These results cannot be changed after saving. Please double check their accuracy.", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                self.save()
            }))
            alert.addAction(UIAlertAction(title: "Do not show again", style: .default, handler: { _ in
                UserDefaults.standard.set(true, forKey: "DontShow")
                self.save()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            self.save()
        }
    }
}

extension Date {
    static var currentTimeStamp: String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMyyyyHH:mm:ss"
        let newDate = dateFormatter.string(from: Date())
        return newDate
    }
}
