//
//  LoginViewController.swift
//  FUTTrackerPro
//
//  Created by Ian Houghton on 01/09/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var reenterEmailAddressView: UIView!
    @IBOutlet weak var reenterPasswordNameView: UIView!
    @IBOutlet var signUpConstraints: [NSLayoutConstraint]!
    @IBOutlet var loginConstraints: [NSLayoutConstraint]!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var reenterEmailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var reenterPasswordTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.segmentedControlSwitched(nil)
        self.segmentedControl.selectedSegmentIndex = 0
        self.hideSignUpFields()
    }
    
    private func showSignUpFields() {
        self.animateForm(false)
    }
    
    private func hideSignUpFields() {
        self.animateForm(true)
    }
    
    private func animateForm(_ show: Bool) {
        UIView.animate(withDuration: 0.15) {
            self.reenterEmailAddressView.isHidden = show
            self.reenterPasswordNameView.isHidden = show
            NSLayoutConstraint.deactivate(show ? self.signUpConstraints : self.loginConstraints)
            NSLayoutConstraint.activate(show ? self.loginConstraints : self.signUpConstraints)
            self.nextButton.setTitle(show ? "Login" : "Sign up", for: .normal)
            self.view.layoutIfNeeded()
        }
    }
    
    private func validateFields() {
        switch self.segmentedControl.selectedSegmentIndex {
        case 0:
            do {
                let email = try self.emailAddressTextField.validatedText(validationType: .email)
                let password = try self.passwordTextField.validatedText(validationType: .password)
                self.signIn(email: email, password: password)
            } catch(let error) {
                self.showAlert(for: (error as! ValidationError).message)
            }
            break
        case 1:
            do {
                let email = try self.emailAddressTextField.validatedText(validationType: .email)
                let reenterEmail = try self.reenterEmailAddressTextField.validatedText(validationType: .email)
                let password = try self.passwordTextField.validatedText(validationType: .password)
                let reenterPassword = try self.reenterPasswordTextField.validatedText(validationType: .password)
                let name = try self.nameTextField.validatedText(validationType: .name)
                
                if email != reenterEmail {
                    self.showAlert(for: "Email addresses don't match")
                    break
                }
                
                if password != reenterPassword {
                    self.showAlert(for: "Passwords don't match")
                    break
                }
                
                self.register(email: email, password: password, name: name)
                
            } catch(let error) {
                self.showAlert(for: (error as! ValidationError).message)
            }
            break
        default:
            break
        }
    }
    
    private func signIn(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error == nil {
                self.dismiss(animated: true, completion: nil)
            }
            else {
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    private func register(email: String, password: String, name: String) {
        
        Auth.auth().createUser(withEmail: email, password: password){ (user, error) in
            if error == nil {
                self.performSegue(withIdentifier: "signupToHome", sender: self)
            } else {
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    private func showAlert(for alert: String) {
        let alertController = UIAlertController(title: nil, message: alert, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }

    @IBAction func segmentedControlSwitched(_ sender: Any?) {
        
        switch self.segmentedControl.selectedSegmentIndex {
        case 0:
            self.hideSignUpFields()
            break
        case 1:
            self.showSignUpFields()
            break
        default:
            break
        }
    }
    
    @IBAction func nextButtonPressed() {
        self.validateFields()
    }
}
