//
//  ImageCaptureViewController.swift
//  FUTTrackerPro
//
//  Created by Ian Houghton on 27/08/2019.
//  Copyright © 2019 YakApps. All rights reserved.
//

import UIKit

class ImageCaptureViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    private var imagePicker = UIImagePickerController()
    private var pickedImage: UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagePicker.delegate = self
    }
    
    @IBAction func getImageButtonPressed() {
        let alert = UIAlertController(title: "Choose image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    private func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
        {
            self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
            
            let mainView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - 220))
            let overlayImageView = UIImageView(image: UIImage(named: "CameraOverlay"))
            overlayImageView.contentMode = .scaleAspectFit
            mainView.addSubview(overlayImageView)
            overlayImageView.transform = overlayImageView.transform.rotated(by: CGFloat(Double.pi / 2))
            overlayImageView.translatesAutoresizingMaskIntoConstraints = false
            mainView.addConstraints([
                NSLayoutConstraint(item: overlayImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 700),
                NSLayoutConstraint(item: overlayImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 500),
                NSLayoutConstraint(item: overlayImageView, attribute: .centerX, relatedBy: .equal, toItem: mainView, attribute: .centerX, multiplier: 1.0, constant: 0),
                NSLayoutConstraint(item: overlayImageView, attribute: .centerY, relatedBy: .equal, toItem: mainView, attribute: .centerY, multiplier: 1.0, constant: 50),
                ])
            mainView.setNeedsDisplay()
            imagePicker.cameraOverlayView = mainView
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "No camera detected", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func openGallary()
    {
        self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.imagePicker.allowsEditing = true
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    // MARK: ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else {
            let alert  = UIAlertController(title: "Something went wrong", message: "Please try again later", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            picker.dismiss(animated: true, completion: nil)
            return
        }
        self.pickedImage = selectedImage
        picker.dismiss(animated: true, completion: nil)
        self.performSegue(withIdentifier: "ImageCaptureToMatchResultsSegue", sender: nil)
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ImageCaptureToMatchResultsSegue" {
            guard let matchResultsViewController = segue.destination as? MatchResultsViewController, let image = self.pickedImage else {
                return
            }
            matchResultsViewController.configure(image)
        }
    }
}
